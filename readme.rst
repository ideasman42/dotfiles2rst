%%%%%%%%%%%%
dotfiles2rst
%%%%%%%%%%%%

Command line utility to convert comments to RST, building an HTML page with `Sphinx <http://www.sphinx-doc.org>`__.

See `this page <http://members.iinet.net.au/~ideasman42/dotemacs/init.html>`__
for an example of this scripts output.

Example Usage::

   dotfiles2rst --output=emacs_html --root=~ lang=lisp comment=";;" ~/.emacs.d/init.el

.. note::

   It's expected this command will be called from a script since it can become quite long
   if you want to document more than one configuration.


Motivation
==========

This utility was written to be a light-weight alternative to using org-mode for writing elisp,
which needs to have the elisp extracted.

Instead this extracts reStructuredText from any kind of configuration files to create a document (using Sphinx).

.. note::

   This tool internally creates a temporary Sphinx project and deletes it on each execution.
   So you don't need to maintain a separate Sphinx project.

.. note::

   Including custom configurations for Sphinx while possible is not required.


Help Text
=========

.. BEGIN HELP TEXT

Output of ``dotfiles2rst --help``

usage::

       dotfiles2rst [-h] --root ROOT_DIR [--build-bundle DIR] [--output DIR]
                    FILEPATH [FILEPATH ...]

positional arguments:
  FILEPATH
                      Input elisp to convert into rst

                      When multiple input arguments are passed,
                      the document will use the first as the master.

optional arguments:
  -h, --help          show this help message and exit
  --root ROOT_DIR     Root directory (all files will be relative to this).
  --build-bundle DIR  Recursively copy files (use to include files with Sphinx).
  --output DIR        Path to write the output to.

dotfiles2rst
  Utility to take configuration files and write out reStructuredText.

  eg::

     dotfiles2rst --output dotfiles_html --input ~/.emacs/init.el

  **Formatting** ::

     ;;
     ;; This is ``reStructuredText``, Add a `Web Link <https://emacs.stackexchange.com>`__.
     ;; A **blank** comment ends the block.
     ;;

     ;; -------------------------------------
     ;; To Visually Define Important Sections
     ;;
     ;; After a Blank Line any number of dashes are ignored
     ;; so *this* will be evaluated as ``reStructuredText`` as well.
     ;;

     ;; This is a normal comment, it won't be interpreted.

     ; Neither will this.

     ;;; Or this.

.. END HELP TEXT
