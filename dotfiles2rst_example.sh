#!/bin/bash
./dotfiles2rst \
	--root=~ \
	--output=./site \
	--build-bundle=~/.config/dotfiles2rst/build_bundle \
	group="Editors" \
	comment=";;" lang=elisp ~/.config/emacs/init.el \
	comment='"' lang=vim ~/.vim/vimrc \
	group="Utilities" \
	comment="#" lang=bash ~/.tmux.conf \
	group="Shells" \
	comment="#" lang=bash \
	~/.bashrc \
	~/.zshrc \
	~/.mkshrc \
	group="Window Managers" \
	comment="--" lang=haskell \
	~/.xmonad/xmonad.hs \
	comment="#" lang=bash \
	~/.i3/config
